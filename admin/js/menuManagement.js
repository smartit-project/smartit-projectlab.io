/*******************************
 * CREATE DX FUNCTION
 *******************************/
/*
 * 트리 뷰를 생성하는 함수입니다.
 */
function createTreeView(selector, items) {
	$(selector).dxTreeView({
		items,
		expandNodesRecursive: false,
		dataStructure: 'tree',
		// width: 550,
		// height: 460,
		displayExpr: 'name',
		onItemClick: function (e) {
			handleMenuClick(e);
			menuItemActive(e);
			menuPathMapping(e);
		},
		onItemRendered: function (e) {
			if (e.itemData.isDirectory) {
				// console.log(e.itemElement);
				// 메뉴 유형이 그룹일경우 해당 아이템에 class 추가
				e.itemElement.addClass('type-group');
			}
		},
	});
}

/*
 * 트리 뷰 항목을 정렬하고 드래그 이벤트를 정의하는 함수입니다.
 */
function createSortable(selector, targetName) {
	$(selector).dxSortable({
		filter: '.dx-treeview-item',
		data: targetName,
		group: 'shared',
		allowDropInsideItem: true,
		allowReordering: true,
		onDragChange(e) {
			if (e.fromComponent === e.toComponent) {
				const $nodes = e.element.find('.dx-treeview-node');
				const isDragIntoChild = $nodes.eq(e.fromIndex).find($nodes.eq(e.toIndex)).length > 0;

				if (isDragIntoChild) {
					e.cancel = true;
				}
			}
		},
		onDragEnd(e) {
			if (e.fromComponent === e.toComponent && e.fromIndex === e.toIndex) {
				return;
			}

			const fromTreeView = $('#treeViewMenu').dxTreeView('instance');
			const toTreeView = $('#treeViewMenu').dxTreeView('instance');

			const fromNode = findNode(fromTreeView, e.fromIndex);
			const toNode = findNode(toTreeView, calculateToIndex(e));

			if (e.dropInsideItem && toNode !== null && !toNode.itemData.isDirectory) {
				return;
			}

			const fromTopVisibleNode = getTopVisibleNode(e.fromComponent);
			const toTopVisibleNode = getTopVisibleNode(e.toComponent);

			const fromItems = fromTreeView.option('items');
			const toItems = toTreeView.option('items');
			moveNode(fromNode, toNode, fromItems, toItems, e.dropInsideItem);

			fromTreeView.option('items', fromItems);
			toTreeView.option('items', toItems);
			fromTreeView.scrollToItem(fromTopVisibleNode);
			toTreeView.scrollToItem(toTopVisibleNode);
		},
	});
}

/*******************************
 * COMMON DX FUNCTION
 *******************************/
/*
 * 트리 뷰 항목의 인덱스를 계산하는 함수입니다.
 */
function calculateToIndex(e) {
	if (e.fromComponent !== e.toComponent || e.dropInsideItem) {
		return e.toIndex;
	}

	return e.fromIndex >= e.toIndex ? e.toIndex : e.toIndex + 1;
}

/*
 * 트리 뷰와 항목 인덱스를 기반으로 항목의 ID를 반환하는 함수입니다.
 */
function findNode(treeView, index) {
	const nodeElement = treeView.element().find('.dx-treeview-node')[index];
	if (nodeElement) {
		return findNodeById(treeView.getNodes(), nodeElement.getAttribute('data-item-id'));
	}
	return null;
}

/*
 * 항목 ID를 기반으로 트리 뷰 노드를 찾는 함수입니다.
 */
function findNodeById(nodes, id) {
	for (let i = 0; i < nodes.length; i += 1) {
		if (nodes[i].itemData.id === id) {
			return nodes[i];
		}
		if (nodes[i].children) {
			const node = findNodeById(nodes[i].children, id);
			if (node != null) {
				return node;
			}
		}
	}
	return null;
}

/*
 * 드래그로 인해 노드의 순서를 변경하는 함수입니다.
 */
function moveNode(fromNode, toNode, fromItems, toItems, isDropInsideItem) {
	/*
	 * fromNode	  : 옮기기 전 부모노드
	 * toNode	  : 옮길 부모노드
	 * fromItems  : 옮기기 전 arrayData
	 * toItems	  : 옮긴 후 arrayData
	 * isDropInsideItem	  : 노드 객체에 직접 드래그하여 drop했을 경우 true
	 * 					    노드 객체의 하위에 drop했을 경우 false
	 */

	// 옮기는 것이 3댑스를 초과하는지 확인

	let toNodeDepth;
	const fromNodeItemsLenght = fromNode.items.length;
	const fromNodeDir = fromNode.itemData.isDirectory;
	if (isDropInsideItem)
		toNodeDepth = toNode.itemData.depth; // 대상 노드의 depth
	else
		toNodeDepth = toNode.parent ? (toNode.parent.itemData.depth || 0) : 0;

	// 이동이 3댑스를 초과하는 경우 경고 메시지를 출력하고 함수 종료
	if (toNodeDepth >= 2 && (fromNodeItemsLenght >= 1 || fromNodeDir)) {
		alert('최대 3댑스까지만 추가할 수 있습니다.');
		return;
	}

	const fromNodeContainingArray = getNodeContainingArray(fromNode, fromItems); // fromNode를 포함하는 배열 가져오기
	const fromIndex = findIndex(fromNodeContainingArray, fromNode.itemData.id); // fromNode의 인덱스 찾기
	fromNodeContainingArray.splice(fromIndex, 1); // fromNode를 배열에서 제거

	if (isDropInsideItem) {
		fromNode.itemData.parentId = toNode.itemData.id;
        fromNode.itemData.depth = toNode.itemData.depth + 1;
		// 노드를 다른 노드 내부에 드롭한 경우, 대상 노드의 items 배열에 추가
		toNode.itemData.items.splice(toNode.itemData.items.length, 0, fromNode.itemData);
	} else {
		fromNode.itemData.parentId = toNode.parent ? toNode.parent.itemData.id : null;
        fromNode.itemData.depth = toNode.parent ? (toNode.parent.itemData.depth + 1) : 1;
		// 그렇지 않은 경우, 대상 노드의 부모 배열에 추가
		const toNodeContainingArray = getNodeContainingArray(toNode, toItems);
		const toIndex = toNode === null ? toItems.length : findIndex(toNodeContainingArray, toNode.itemData.id);
		toNodeContainingArray.splice(toIndex, 0, fromNode.itemData);
	}
}

/*
 * 노드가 포함된 배열을 반환하는 함수입니다.
 */
function getNodeContainingArray(node, rootArray) {
	return node === null || node.parent === null ? rootArray : node.parent.itemData.items;
}

/*
 * 배열에서 특정 ID를 가진 항목의 인덱스를 반환하는 함수입니다.
 */
function findIndex(array, id) {
	const idsArray = array.map(elem => elem.id);
	return idsArray.indexOf(id);
}

/*
 * 트리 뷰에서 가장 상단에 보이는 노드를 반환하는 함수입니다.
 */
function getTopVisibleNode(component) {
	const treeViewElement = component.element().get(0);
	const treeViewTopPosition = treeViewElement.getBoundingClientRect().top;
	const nodes = treeViewElement.querySelectorAll('.dx-treeview-node');
	for (let i = 0; i < nodes.length; i += 1) {
		const nodeTopPosition = nodes[i].getBoundingClientRect().top;
		if (nodeTopPosition >= treeViewTopPosition) {
			return nodes[i];
		}
	}

	return null;
}

/*******************************
 * 메뉴 데이터 배열입니다.
 *******************************/
const itemsMenu = [
	{
		id: '1',
		parentId: '',
		name: '데이터 검색',
		description: '데이터를 검색하기위한 메뉴입니다.',
		isActive: 'Y',
		expanded: true,
		type: 'GROUP',
		isDirectory: true,
		depth: 1,
		items: [
			{
				id: '1-1',
				parentId: '1',
				name: 'SELF BI',
				description: '',
				isActive: 'Y',
				expanded: true,
				type: 'GROUP',
				isDirectory: true,
				depth: 2,
				items: [
					{
						id: '1-1-1',
						parentId: '1-1',
						name: 'SELF BI',
						description: '',
						isActive: 'Y',
						expanded: true,
						type: 'select',
						isDirectory: false,
						depth: 3,
					},
					{
						id: '1-1-2',
						parentId: '1-1',
						name: 'SELF BI',
						description: '',
						isActive: 'Y',
						expanded: true,
						type: 'select',
						isDirectory: false,
						depth: 3,
					},
				],
			},
			{
				id: '1-2',
				parentId: '1',
				name: '업무데이터',
				description: '',
				isActive: 'Y',
				expanded: true,
				type: 'select',
				isDirectory: false,
				depth: 2,
				items: [],
			},
		],
	},
	{
		id: '2',
		parentId: '',
		name: '통계분석',
		description: '',
		isActive: 'Y',
		expanded: true,
		type: 'select',
		isDirectory: false,
		depth: 1,
		items: [],
	},
	{
		id: '3',
		parentId: '',
		name: '데시보드',
		description: '',
		isActive: 'Y',
		expanded: true,
		type: 'select',
		isDirectory: false,
		depth: 1,
		items: [],
	},
	{
		id: '4',
		parentId: '',
		name: '데이터서비스',
		description: '',
		isActive: 'Y',
		expanded: true,
		type: 'GROUP',
		isDirectory: true,
		depth: 1,
		items: [
			{
				id: '4-1',
				parentId: '4',
				name: 'SELF BI',
				description: '',
				isActive: 'Y',
				expanded: true,
				type: 'select',
				isDirectory: false,
				depth: 2,
				items: [],
			},
			{
				id: '4-2',
				parentId: '4',
				name: '업무데이터',
				description: '',
				isActive: 'Y',
				expanded: true,
				type: 'select',
				isDirectory: false,
				depth: 2,
				items: [],
			},
			{
				id: '4-3',
				parentId: '4',
				name: '항공기 대시보드',
				description: '',
				isActive: 'Y',
				expanded: true,
				type: 'select',
				isDirectory: false,
				depth: 2,
				items: [],
			},
			{
				id: '4-4',
				parentId: '4',
				name: '여객흐름분석',
				description: '',
				isActive: 'Y',
				expanded: true,
				type: 'select',
				isDirectory: false,
				depth: 2,
				items: [],
			},
		],
	},
	{
		id: '5',
		parentId: '',
		name: '마이페이지',
		description: '',
		isActive: 'Y',
		expanded: true,
		type: 'select',
		isDirectory: false,
		depth: 1,
		items: [],
	},
];
