'use strict';
// 인풋, 텍스트 글자수 카운트
let setTextCount = function (id, max) {
	let el = document.getElementById(id);
	el.addEventListener('keyup', function (e) {
		e.stopPropagation();
		if (Number(el.value.length) > max) {
			el.value = el.value.substr(0, max);
			alert('글자 수가' + max.toLocaleString() + '자를 초과했습니다.');
		}
		let txtCount = Number(el.value.length).toLocaleString() + '/' + max.toLocaleString();
		el.nextElementSibling.querySelector('.count-txt').innerText = txtCount;
	});
};

let setSearchToggle = function setSearchToggle() {
	var icon = document.getElementById('searchIcon');
	var search = document.getElementById('search');
	var input = document.getElementById('algoliaSearch');

	if (!icon) {
		return;
	}

	icon.addEventListener('click', function (event) {
		search.classList.toggle('bd-is-visible');

		if (search.classList.contains('bd-is-visible')) {
			algoliaSearch.focus();
		}
	});

	window.addEventListener('keydown', function (event) {
		if (event.key === 'Escape') {
			return search.classList.remove('bd-is-visible');
		}
	});
};

// DOMContentLoaded
document.addEventListener('DOMContentLoaded', function () {
	// allmenu, allmenu-open
	if (document.querySelector('.header-util-allmenu')) {
		const btnAllmenu = document.querySelector('.btn-allmenu');
		const allmenuEl = getSiblings(btnAllmenu);
		btnAllmenu.addEventListener('click', function (e) {
			// console.log(allmenuEl);
			// if (allmenuEl.classList.contains('is-active')) {}
			allmenuEl[0].classList.toggle('is-active');
			allmenuEl[0].querySelector('.btn-close').addEventListener('click', function (e) {
				allmenuEl[0].classList.remove('is-active');
			});
		});
	}

	// gnb-list
	if (document.querySelector('.gnb-list')) {
		const gnbList = document.querySelector('.gnb-list');
		const btnMenu = gnbList.querySelectorAll('.btn-menu');
		btnMenu.forEach($el => {
			$el.addEventListener('click', function (e) {
				// console.log(e.target.parentNode.classList);
				e.target.parentNode.classList.toggle('is-active');
			});
		});
	}

	// Dropdowns
	var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
	const elDropItem = getAll('.dropdown-item:not([disabled])');
	const ctbox = document.querySelectorAll('.list-category');
	const ctTitle = document.querySelector('.category-box__title');
	const setDropdowns = () => {
		if ($dropdowns.length > 0) {
			$dropdowns.forEach($el => {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();
					let $item = event.target.closest('.dropdown');
					let $itemActive = $item.classList.contains('is-active');
					if ($itemActive) {
						$item.classList.remove('is-active');
					} else {
						closeDropdowns();
						$item.classList.add('is-active');
					}
				});
			});
			document.addEventListener('click', function (event) {
				closeDropdowns();
			});
			elDropItem.forEach($el => {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();
					getSiblings($el).forEach(function (subel, i) {
						subel.classList.remove('is-active');
					});
					$el.classList.add('is-active');
					if (closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').classList.contains('list')) {
						ctTitle.innerText = $el.innerText;
					} else if (closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').classList.contains('list-product')) {
						const $trigger = document.querySelector('[data-target="foru');
						closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText = $el.innerText;
						const item = getAll('.show-item');
						if ($trigger.innerText == closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText) {
							item.forEach(function (ele) {
								if (ele.classList.contains('is-pagination')) {
									ele.classList.remove('is-block');
								} else if (ele.classList.contains('is-button')) {
									ele.classList.add('is-block');
								}
							});
						} else {
							item.forEach(function (ele) {
								if (ele.classList.contains('is-button')) {
									ele.classList.remove('is-block');
								} else if (ele.classList.contains('is-pagination')) {
									ele.classList.add('is-block');
								}
							});
						}
					} else {
						closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText = $el.innerText;
					}
					closeDropdowns();
				});
			});
		}
	};

	setSearchToggle();
	setDropdowns();

	// scroll to top
	if (document.querySelector('.footer-top-area')) {
		let toTop = document.querySelector('.footer-top-area');

		toTop.addEventListener('click', function (e) {
			e.preventDefault();
			window.scroll({top: 0, left: 0, behavior: 'smooth'});
		});
	}

	function closeDropdowns() {
		$dropdowns.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}

	document.addEventListener('keydown', function (event) {
		var e = event || window.event;

		if (e.key === 'Escape') {
			let dropdownActive = document.querySelector('.dropdown.is-active');
			if (dropdownActive) {
				closeDropdowns();
			}
		}
	});

	// Spinner box
	var setUiSpinner = function uiSpinner() {
		const $spinner = document.querySelectorAll('[data-js-spinner]');

		Array.prototype.forEach.call($spinner, function ($element) {
			const $input = $element.querySelector('[data-js-spinner-input]');
			const $btnDecrement = $element.querySelector('[data-js-spinner-decrement]');
			const $btnIncrement = $element.querySelector('[data-js-spinner-increment]');
			const inputRules = {
				min: +$input.getAttribute('min'),
				max: +$input.getAttribute('max'),
				steps: +$input.getAttribute('steps') || 1,
				maxlength: +$input.getAttribute('maxlength') || null,
				minlength: +$input.getAttribute('minlength') || null,
			};
			let inputValue = +$input.value || 0;

			$input.addEventListener('input', handleInputUpdateValueInput, false);
			$element.addEventListener('keydown', handleMousedownDecrementSpinner, false);
			$btnDecrement.addEventListener('click', handleClickDecrementBtnDecrement, false);
			$btnIncrement.addEventListener('click', handleClickIncrementBtnIncrement, false);

			function handleInputUpdateValueInput() {
				let value = +$input.value;

				if (isNaN(value)) inputValue = 0;
				else inputValue = value;
			}

			function handleMousedownDecrementSpinner(event) {
				const keyCode = event.keyCode;
				const arrowUpKeyCode = 38;
				const arrowDownKeyCode = 40;

				if (keyCode === arrowDownKeyCode) handleClickDecrementBtnDecrement();
				else if (keyCode === arrowUpKeyCode) handleClickIncrementBtnIncrement();
			}

			function handleClickDecrementBtnDecrement() {
				if (!isGreaterThanMaxlength(inputValue - 1)) {
					if ($input.hasAttribute('min')) {
						if (inputValue > inputRules.min) decrement();
					} else decrement();
				}
			}

			function handleClickIncrementBtnIncrement() {
				if (!isGreaterThanMaxlength(inputValue + 1)) {
					if ($input.hasAttribute('max')) {
						if (inputValue < inputRules.max) increment();
					} else increment();
				}
			}

			function decrement() {
				inputValue -= inputRules.steps;
				if ($input.hasAttribute('max') && inputValue > $input.getAttribute('max')) {
					inputValue = +$input.getAttribute('max');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function increment() {
				inputValue += inputRules.steps;
				if ($input.hasAttribute('min') && inputValue < $input.getAttribute('min')) {
					inputValue = +$input.getAttribute('min');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function isGreaterThanMaxlength(value) {
				return value.toString().length > inputRules.maxlength && inputRules.maxlength !== null;
			}
		});
	};

	setUiSpinner();

	// Collapse / Accordion
	function initCollapse(elem, option) {
		document.addEventListener('click', function (e) {
			if (!e.target.matches(elem + ' [data-collapse]')) return;
			else {
				e.preventDefault();
				let parentEl = e.target.parentElement.closest('.collapse-item');
				if (parentEl.classList.contains('is-active')) {
					parentEl.classList.remove('is-active');
				} else {
					// accordion
					if (option == true) {
						let accId = parentEl.getAttribute('data-accordion');
						let accEl = document.getElementById(accId);
						let elementList = accEl.querySelectorAll('.collapse-item');
						Array.prototype.forEach.call(elementList, function (e) {
							e.classList.remove('is-active');
						});
					}
					parentEl.classList.add('is-active');
				}
			}
		});
	}
	initCollapse('.collapse.is-accordion', true);
	initCollapse('.collapse.is-collapse', false);

	// search modal popup action
	let elHKeyword = document.querySelectorAll('.keyword-header-search');
	if (elHKeyword) {
		elHKeyword.forEach(function (el, key) {
			el.addEventListener('click', function (event) {
				event.stopPropagation();
				document.querySelector('.header-search-input').value = this.innerText;
			});
		});
	}
	let elHKwDelete = document.querySelectorAll('.button-delete-hkeyword');
	if (elHKwDelete) {
		elHKwDelete.forEach(function (el, key) {
			el.addEventListener('click', function (event) {
				event.stopPropagation();
				closest(this, 'header-search-layer__recent-item').remove();
			});
		});
	}
	let elHKwDelAll = document.querySelector('.button-delete-hkeyword__all');
	if (elHKwDelAll) {
		elHKwDelAll.addEventListener('click', function (event) {
			event.stopPropagation();
			let elHKwItem = document.querySelectorAll('.header-search-layer__recent-item');
			if (elHKwItem) {
				elHKwItem.forEach(function (el, key) {
					el.remove();
				});
			}
		});
	}

	// password container
	let elPassword = document.querySelectorAll('.password-container');
	if (elPassword) {
		elPassword.forEach(function (el, key) {
			el.querySelector('.btn-onoff').addEventListener('click', function (event) {
				event.stopPropagation();
				this.classList.toggle('is-active');
				let passwordInput = el.querySelector('.input');
				if (this.classList.contains('is-active')) {
					passwordInput.type = 'text';
				} else {
					passwordInput.type = 'password';
				}
			});
		});
	}

	// File upload : Dropzone js
	if (document.querySelector('.file')) {
		// 	<img data-dz-thumbnail="data-dz-thumbnail" class="" src="#" alt="Dropzone-Image" style="width: 120px;"/>
		// <p class="size" data-dz-size="data-dz-size"></p>
		var previewTemplate = `
			<div class="file-preview-item">
				<div class="file-preview-item-box">
					<div class="name" data-dz-name="data-dz-name">&nbsp;</div>
					<button class="btn-delete" data-dz-remove="data-dz-remove"><span class="sr-only">Delete</span></button>
				</div>
				<strong class="error" data-dz-errormessage="data-dz-errormessage"></strong>
			</div>
			`;
		const file = document.querySelectorAll('.file');

		file.forEach($el => {
			const dropzoneEl = $el.querySelector('.file-zone');
			const dropzoneList = $el.querySelector('.file-preview');
			Dropzone.autoDiscover = false;

			const dropzone = new Dropzone(dropzoneEl, {
				url: 'https://url', // 파일을 업로드할 서버 주소 url.
				method: 'post', // 기본 post로 request 감. put으로도 할수있음
				maxFiles: 5,
				// maxFilesize: ,
				previewTemplate: previewTemplate, // Custom Preview List Theme
				previewsContainer: dropzoneList, // Preview List Container
			});
		});
	}

	// 체크박스 전체 선택 및 해제
	if (document.querySelector('.all-checkbox')) {
		let $chk = false;
		let $checked = getAll('.all-checkbox');
		if ($checked.length > 0) {
			$checked.forEach(function ($el) {
				$el.addEventListener('click', function () {
					var chk = $el.checked;
					var parent = $el.closest('.table');
					var target = parent.querySelectorAll('.all-checkbox-item');
					//console.log(chk);
					checkAll(target, chk);
				});
			});
		}
		function checkAll(target, chk) {
			$chk = $chk ? false : true;
			for (var i = 0; i < target.length; i++) {
				target[i].checked = chk;
				//console.log($chk);
			}
		}
	}
});

// 웹접근성 Dynamic Tab
function TabsAutomatic(groupNode) {
	this.tablistNode = groupNode;
	this.tabs = [];
	this.firstTab = null;
	this.lastTab = null;
	// ie11
	let tabs = [];
	new Set(this.tablistNode.querySelectorAll('[role=tab]')).forEach(function (v) {
		tabs.push(v);
	});
	this.tabs = tabs;
	this.tabpanels = [];

	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		var tabpanel = document.getElementById(tab.getAttribute('aria-controls'));

		tab.tabIndex = -1;
		tab.setAttribute('aria-selected', 'false');
		this.tabpanels.push(tabpanel);
		tab.addEventListener('keydown', this.onKeydown.bind(this));
		tab.addEventListener('click', this.onClick.bind(this));

		if (!this.firstTab) {
			this.firstTab = tab;
		}
		this.lastTab = tab;
	}
	this.setSelectedTab(this.firstTab, false);
}
TabsAutomatic.prototype.setSelectedTab = function (currentTab, setFocus) {
	if (typeof setFocus !== 'boolean') {
		setFocus = true;
	}
	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		if (currentTab === tab) {
			tab.setAttribute('aria-selected', 'true');
			tab.parentElement.classList.add('is-active');
			tab.removeAttribute('tabindex');
			this.tabpanels[i].setAttribute('aria-hidden', 'false');
			this.tabpanels[i].classList.add('is-active');
			if (setFocus) {
				tab.focus();
			}
		} else {
			tab.parentElement.classList.remove('is-active');
			tab.setAttribute('aria-selected', 'false');
			tab.tabIndex = -1;
			this.tabpanels[i].setAttribute('aria-hidden', 'true');
			this.tabpanels[i].classList.remove('is-active');
		}
	}
};
TabsAutomatic.prototype.setSelectedToPreviousTab = function (currentTab) {
	var index;

	if (currentTab === this.firstTab) {
		this.setSelectedTab(this.lastTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index - 1]);
	}
};
TabsAutomatic.prototype.setSelectedToNextTab = function (currentTab) {
	var index;

	if (currentTab === this.lastTab) {
		this.setSelectedTab(this.firstTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index + 1]);
	}
};
/* EVENT HANDLERS */
TabsAutomatic.prototype.onKeydown = function (event) {
	var tgt = event.currentTarget,
		flag = false;

	switch (event.key) {
		case 'ArrowLeft':
			this.setSelectedToPreviousTab(tgt);
			flag = true;
			break;

		case 'ArrowRight':
			this.setSelectedToNextTab(tgt);
			flag = true;
			break;

		case 'Home':
			this.setSelectedTab(this.firstTab);
			flag = true;
			break;

		case 'End':
			this.setSelectedTab(this.lastTab);
			flag = true;
			break;

		default:
			break;
	}

	if (flag) {
		event.stopPropagation();
		event.preventDefault();
	}
};
TabsAutomatic.prototype.onClick = function (event) {
	this.setSelectedTab(event.currentTarget);
};
// Initialize tablist
window.addEventListener('load', function () {
	var tablists = document.querySelectorAll('ul[role=tablist]');
	for (var i = 0; i < tablists.length; i++) {
		new TabsAutomatic(tablists[i]);
	}
});

// Utils
function getAll(selector) {
	var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

	return Array.prototype.slice.call(parent.querySelectorAll(selector), 0);
}
// get all siblings
function getSiblings(e) {
	// for collecting siblings
	let siblings = [];
	// if no parent, return no sibling
	if (!e.parentNode) {
		return siblings;
	}
	// first child of the parent node
	let sibling = e.parentNode.firstChild;
	// collecting siblings
	while (sibling) {
		if (sibling.nodeType === 1 && sibling !== e) {
			siblings.push(sibling);
		}
		sibling = sibling.nextSibling;
	}
	return siblings;
}
function extend(a, b) {
	for (var key in b) {
		if (b.hasOwnProperty(key)) {
			a[key] = b[key];
		}
	}
	return a;
}
function hasParent(e, id) {
	if (!e) return false;
	var el = e.target || e.srcElement || e || false;
	while (el && el.id != id) {
		el = el.parentNode || false;
	}
	return el !== false;
}
// returns the depth of the element "e" relative to element with id=id
// for this calculation only parents with classname = waypoint are considered
function getLevelDepth(e, id, waypoint, cnt) {
	cnt = cnt || 0;
	if (e.id.indexOf(id) >= 0) return cnt;

	if (e.classList.contains(waypoint)) {
		++cnt;
	}
	return e.parentNode && getLevelDepth(e.parentNode, id, waypoint, cnt);
}
// returns the closest element to 'e' that has class "classname"
function closest(e, classname) {
	if (e.classList.contains(classname)) {
		return e;
	}
	return e.parentNode && closest(e.parentNode, classname);
}

// Toast message
function displayToast(position, type, msg) {
	bulmaToast.toast({
		message: msg,
		type: type,
		position: position.toLowerCase().replace(' ', '-'),
		dismissible: true,
		duration: 2000,
		pauseOnHover: true,
		animate: {in: 'fadeIn', out: 'fadeOut'},
	});
}

// item drag and drop : Sortable.js
// dragAndDrop('drop-items', '.btn-item-drag');
function dragAndDrop(valueId, classHandle) {
	const dropItems = document.getElementById(valueId);
	new Sortable(dropItems, {
		animation: 350,
		handle: classHandle,
		ghostClass: 'is-ghost',
		// chosenClass: "sortable-chosen",
		// dragClass: "sortable-drag"
	});
}

// multi event 함수 ex. addListenerMulti(window, 'mousemove touchmove', function(){…});
function addListenerMulti(el, s, fn) {
	s.split(' ').forEach(e => el.addEventListener(e, fn, false));
}
